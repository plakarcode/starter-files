<?php
/**
 * Heading H3
 *
 * Template part for rendering ACF flexible sections - heading h3
 *
 * Used in flexible-templates/
 *         - sections.php
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
/**
 * Translators: field id, echo (false returns value), before, after
 */
acf_sub_field( 'heading', true, '<h3>', '</h3>' );