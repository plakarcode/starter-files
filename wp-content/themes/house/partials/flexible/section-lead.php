<?php
/**
 * Leading paragraph
 *
 * Template part for rendering ACF flexible sections - leading paragraph
 *
 * Used in flexible-templates/
 *         - sections.php
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
/**
 * Translators: field id, echo (false returns value), before, after
 */
acf_sub_field( 'lead', true, '<p class="text-highlighted">', '</p>' );
