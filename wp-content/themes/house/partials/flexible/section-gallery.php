<?php
/**
 * Gallery
 *
 * Template part for rendering ACF flexible sections - gallery
 *
 * Used in flexible-templates/
 *         - sections.php
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
/**
 * Translators: field id, echo (false returns value), before, after
 */
$gallery = acf_sub_field( 'gallery', false );

if ( $gallery ) {
	foreach ( $gallery as $image ) :

		/**
		 * Prepare sizes
		 * @var string
		 */
		$thumbnail = $image['sizes']['thumbnail'];
		$medium    = $image['sizes']['medium'];
		$large     = $image['sizes']['large'];
		$full      = $image['url'];	?>

		<img src="<?php echo $thumbnail; ?>" >

	<?php endforeach; // $gallery as $image
}
