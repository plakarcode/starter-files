<?php
/**
 * Author
 *
 * Template part for rendering author's info on posts
 * if a user has filled out their description and
 * this is a multi-author blog.
 *
 * @package WordPress
 */

if ( is_singular() && get_the_author_meta( 'description' ) && is_multi_author() ) : ?>
<div class="author-info">
	<div class="author-avatar">
		<?php echo get_avatar( get_the_author_meta( 'user_email' ), apply_filters( 'house_author_bio_avatar_size', 68 ) ); ?>
	</div><!-- .author-avatar -->
	<div class="author-description">
		<div class="author-about">
			<h3><?php printf( __( 'About %s', 'house' ), get_the_author() ); ?></h3>
			<p><?php the_author_meta( 'description' ); ?></p>
			<a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author" class="author-page-link"><?php printf( __( 'View all posts by %s', 'house' ), get_the_author() . ' <span class="meta-nav">&rarr;</span>' ); ?></a>
		</div>
	</div><!-- .author-description -->
</div><!-- .author-info -->
<?php endif; ?>