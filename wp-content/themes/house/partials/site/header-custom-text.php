<?php
/**
 * Header custom text
 *
 * Template part for rendering custom content for header.
 * This can be populated from customizer.
 *
 * @package WordPress
 */
?>

<div class="header-custom-text">
	<?php
		/**
		 * Get custom header text from customizer
		 */
		if ( function_exists( 'get_theme_mod' ) || is_customize_preview() ) {
			echo get_theme_mod( 'header_custom_text' );
		}
	?>
</div><!-- header-custom-text -->