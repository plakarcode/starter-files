<?php
/**
 * Global branding
 *
 * Template part for rendering site title, description and/or logo.
 *
 * @link https://developer.wordpress.org/reference/functions/the_custom_logo/
 * @link https://developer.wordpress.org/reference/functions/is_customize_preview/
 *
 * @package WordPress
 */
/**
 * Get site logo and/or
 * site title and tagline
 */
if ( function_exists( 'the_custom_logo' ) ) {
	the_custom_logo();
}
/**
 * Get site title
 * <h1> on home and <p> and other pages
 */
if ( is_front_page() || is_home() ) :
	house_site_title_link( '<h1 class="site-title">', '</h1>' );
else :
	house_site_title_link( '<p class="site-title">', '</p>' );
endif;

/**
 * Get the description
 * @var string
 */
$description = get_bloginfo( 'description', 'display' );

if ( $description || is_customize_preview() ) : ?>
	<p class="site-description"><?php echo $description; ?></p>
<?php endif;