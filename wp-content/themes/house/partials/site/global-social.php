<?php
/**
 * Global social
 *
 * Template part for rendering social profile links.
 *
 * @package WordPress
 */

/**
 * Get all populated social links from customizer
 *
 * There are other helper functions available, e.g. get specific
 * profile link or just url etc. @see inc/branding/social.php
 */
if ( function_exists( 'get_theme_mod' ) || is_customize_preview() ) {
	all_social_profiles();
}