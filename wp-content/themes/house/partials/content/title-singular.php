<?php
/**
 * Singular titles
 *
 * Template part for rendering titles for posts and pages
 *
 * @package WordPress
 */

/**
 * Singular - post or page but not home page
 */
if ( is_singular() && ! is_front_page() ) :

	the_title( '<h1 class="entry-title">', '</h1>' );

/**
 * On archive pages (for posts)
 */
elseif ( ! is_singular() ) :

	$title = esc_attr( sprintf( __( 'Permalink to %s', 'house' ), the_title_attribute( 'echo=0' ) ) );
	the_title( sprintf( '<h2 class="entry-title"><a href="%1$s" title="%2$s" rel="bookmark">', esc_url( get_permalink() ), $title ), '</a></h2>' );

endif;