<?php
/**
 * Main navigation template part
 *
 * Template part for rendering main navigation.
 *
 * @link https://codex.wordpress.org/Function_Reference/wp_nav_menu
 *
 * @package WordPress
 */
?>
<nav id="site-navigation" class="main-navigation" role="navigation">
	<a class="assistive-text" href="#content" title="<?php esc_attr_e( 'Skip to content', 'house' ); ?>"><?php _e( 'Skip to content', 'house' ); ?></a>
	<?php wp_nav_menu( array( 'theme_location' => 'header' ) ); ?>
</nav><!-- #site-navigation -->