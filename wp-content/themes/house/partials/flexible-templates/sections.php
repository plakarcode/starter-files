<?php
/**
 * Flexible sections
 *
 * Template part for rendering ACF flexible sections
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
/**
 * Define flexible field ID
 * @var string
 */
$flexible_field = 'all_content_fields';
/**
 * Define path to template parts
 * @var string
 */
$path = 'partials/flexible/section';
/**
 * Define fields
 * @var array
 */
$templates = [

	// h1
	'heading_h1' => [
		'dir'      => $path,
		'template' => 'heading-h1',
	],
	// h2
	'heading_h2' => [
		'dir'      => $path,
		'template' => 'heading-h2',
	],
	// h3
	'heading_h3' => [
		'dir'      => $path,
		'template' => 'heading-h3',
	],
	// h4
	'heading_h4' => [
		'dir'      => $path,
		'template' => 'heading-h4',
	],

	// lead paragraph
	'lead' => [
		'dir'      => $path,
		'template' => 'lead',
	],

	// regular paragraph
	'paragraph' => [
		'dir'      => $path,
		'template' => 'paragraph',
	],

	// textarea with 'Automatically add paragraphs' formating
	'content' => [
		'dir'      => $path,
		'template' => 'content',
	],

	// blockquote
	'blockquote' => [
		'dir'      => $path,
		'template' => 'blockquote',
	],

	// list - ul and ol
	'list' => [
		'dir'      => $path,
		'template' => 'list',
	],

	// image - size full, all sizes ready
	'image' => [
		'dir'      => $path,
		'template' => 'image',
	],

	// gallery
	'gallery' => [
		'dir'      => $path,
		'template' => 'gallery',
	],

	// hr
	'hr' => [
		'dir'      => $path,
		'template' => 'hr',
	],
];

/**
 * Start the loop
 */
while ( the_flexible_field( $flexible_field ) ) :

	foreach ( $templates as $id => $t ) :

		if ( get_row_layout() == $id ) :

			get_template_part( $t['dir'], $t['template'] );

		endif; // get_row_layout()

	endforeach; // $templates as $id => $t

endwhile; // the_flexible_field( $flexible_field )