<?php
/**
 * The Header
 *
 * Displays all of the <head> section
 *
 * @todo set logo image
 *
 * @package WordPress
 */
?><!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?> class="<?php house_html_class(); ?> no-js lt-ie9 lt-ie8 lt-ie7 ie6">
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?> class="<?php house_html_class(); ?> no-js lt-ie9 lt-ie8 ie7">
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?> class="<?php house_html_class(); ?> no-js lt-ie9 ie8">
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?> class="<?php house_html_class(); ?> no-js">
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1" />
<link rel="profile" href="http://gmpg.org/xfn/11" /><?php
global $wp, $globalSite;
$current_url = home_url( add_query_arg( array(), $wp->request ) . '/');
$sitename = get_bloginfo( 'name' );
$sitedescription = get_bloginfo( 'description' );
$type = '';
if ( is_front_page() ) {
	$type = 'website';
} else {
	$type = 'article';
} ?>
<script type="application/ld+json">
{
	"@context"    : "http://schema.org",
	"@type"       : "Organization",
	"name"        : "<?php echo $sitename; ?>",
	"description" : "<?php echo html_entity_decode( $sitedescription ); ?>",
	"url"         : "<?php echo $globalSite['home']; ?>",
	"logo"        : "<?php echo $globalSite['template_url']; ?>/images/logo.jpg"
}
</script>
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> >

	<!--[if lt IE 9]>
		<div class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</div>
	<![endif]-->

	<header id="mainheader" class="header-main" role="banner">
		<div class="container">
			<?php
				/**
				 * Get header custom text
				 */
				get_template_part( 'partials/site/header', 'custom-text' );
				/**
				 * Get social profiles links
				 */
				get_template_part( 'partials/site/global', 'social' );
				/**
				 * Get site logo and/or
				 * site title and tagline
				 */
				get_template_part( 'partials/site/global', 'branding' );

				/**
				 * Get main naivation
				 */
				get_template_part( 'partials/navigations/header' );
			?>
		</div><!-- container -->
	</header><!-- #mainheader -->

	<main id="content" class="content-main" role="main">