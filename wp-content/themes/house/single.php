<?php
/**
 * Single
 *
 * The Template for displaying all single posts.
 *
 * @package WordPress
 */
get_header();

	/**
	 * Get the featured image
	 * if one is set
	 */
	if ( has_post_thumbnail() ) {
		/**
		 * Translators: image size, string or array of attributes
		 */
		the_post_thumbnail( 'full', array( 'alt' => the_title_attribute( 'echo=0' ) ) );
	}

	// start loop
	while ( have_posts() ) : the_post();

		/**
		 * If we have template part for post format and
		 * we are on post format single.
		 *
		 * WordPress will first look for 'format-FORMAT_NAME.php',
		 * if none found fallback is 'format.php'. If that one is missing as well,
		 * it'll look for 'content.php'
		 */
		if ( has_post_format( get_post_format() ) ) {
			get_template_part( 'format', get_post_format() );
		}
		/**
		 * If we have template part for custom post type and
		 * we are on custom post type single.
		 *
		 * WordPress will first look for 'content-CPT_NAME.php',
		 * if none found fallback is 'content.php'.
		 */
		elseif ( post_type_exists( get_post_type() ) ) {
			get_template_part( 'content', get_post_type() );
		}
		/**
		 * If, in any case, none from above applies
		 */
		else {
			get_template_part( 'content' );
		}

		/**
		 * Get the comments list and form
		 * @link https://developer.wordpress.org/reference/functions/comments_template/
		 */
		comments_template( '', true );

	endwhile; // end of the loop.

get_footer();