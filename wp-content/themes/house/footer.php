<?php
/**
 * The footer
 *
 * Contains footer content and the closing of the
 * body and html.
 *
 * @package WordPress
 */
?>
	</main><!-- #content -->

	<footer id="mainfooter" class="footer-main" role="contentinfo">
		<div class="container">
			<?php
				/**
				 * Get footer naivation
				 */
				get_template_part( 'partials/navigations/footer' );
			?>

			<div id="colophon">
				<?php
					/**
					 * Get footer copyrights
					 */
					$default = get_house_footer_copyrights();
					if ( function_exists( 'get_theme_mod' ) || is_customize_preview() ) {
						echo get_theme_mod( 'footer_copyright_text', $default );
					}
				?>
			</div><!-- #colophon -->

		</div><!-- container -->
	</footer><!-- #mainfooter -->

<?php wp_footer(); ?>
</body>
</html>