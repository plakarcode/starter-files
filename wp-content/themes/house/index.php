<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 */
get_header(); ?>

	<?php if ( have_posts() ) :?>

		<?php
		/* Start the Loop */
		while ( have_posts() ) : the_post();

			/**
			 * If we have template part for post format and
			 * we are on post format single.
			 *
			 * WordPress will first look for 'format-FORMAT_NAME.php',
			 * if none found fallback is 'format.php'. If that one is missing as well,
			 * it'll look for 'content.php'
			 */
			if ( has_post_format( get_post_format() ) ) {
				get_template_part( 'format', get_post_format() );
			}
			/**
			 * If we have template part for custom post type and
			 * we are on custom post type single.
			 *
			 * WordPress will first look for 'content-CPT_NAME.php',
			 * if none found fallback is 'content.php'.
			 */
			elseif ( post_type_exists( get_post_type() ) ) {
				get_template_part( 'content', get_post_type() );
			}
			/**
			 * If, in any case, none from above applies
			 */
			else {
				get_template_part( 'content' );
			}

		endwhile; ?>

		<?php
			/**
			 * Get pagination
			 */
			if ( function_exists( 'house_content_pagination' ) ) {
				house_content_pagination( 'pagination' );
			}
		?>

	<?php else : ?>
		<?php get_template_part( 'content', 'none' ); ?>
	<?php endif; // end have_posts() check ?>

<?php get_footer(); ?>