<?php
/**
 * Register all sidebars used in theme
 *
 * This function is attached to 'widgets_init' action hook.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 * @link http://codex.wordpress.org/Function_Reference/register_widget
 * @package WordPress
 */

add_action( 'widgets_init', 'house_widgets_init' );

function house_widgets_init() {

	register_sidebar( array(
		'name' => __( 'Main Sidebar', 'house' ),
		'id' => 'main-sidebar',
		'description' => __( 'Main widget area', 'house' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
}


