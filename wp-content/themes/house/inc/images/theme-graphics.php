<?php
/**
 * Get theme graphics
 *
 * Functions for getting all theme graphics - 'images' and 'icons' folders
 *
 * @package WordPress
 */
/**
 * Image
 *
 * Get images located in theme's 'images' folder
 *
 * @param  string $image 	File name - or path from /images/ dir
 * @param  string $class 	Class for the image
 * @param  string $alt   	Alt for the image
 * @return string        	Returns image markup
 */
function house_image( $image = '', $class = '', $alt = '' ) {
	global $globalSite;

	$output = '<img src="' . $globalSite['template_url'] . '/images/' . $image . '" class="' . $class . '" alt="' . $alt . '" />';

	return $output;
}
/**
 * SVG Icon
 *
 * Get the svg icon located in theme's 'icons' folder
 *
 * @param  string $icon 		Name of icon
 * @param  string $icon_class 	Class for the icon
 * @return string       		Returns icon markup
 */
function house_svg_icon( $icon = '', $icon_class = '' ) {
	global $globalSite;

	$output = '';

	$output .= '<svg role="img" class="icon icon-' . $icon . ' ' . $icon_class . '">';
	$output .= '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="' . $globalSite['template_url'] . '/icons/icons.svg#icon-' . $icon . '"></use>';
	$output .= '</svg>';

	return $output;
}
/**
 * SVG icons in links
 *
 * @uses  house_svg_icon()
 *
 * @param  string $icon 		Name of icon
 * @param  string $url  		URL for the link
 * @param  string $icon_class 	Class for the icon
 * @param  string $link_class 	Class for the link
 * @param  string $target 		Link target attribute, default is self
 * @return string       		Returns link markup
 */
function house_svg_icon_link( $icon = '', $url = '', $icon_class = '', $link_class = '', $target = '_self' ) {
	$svg = house_svg_icon( $icon, $icon_class );

	$output = '<a href="' . $url . '" class="' . $link_class . '" target="' . $target . '">' . $svg . '</a>';

	return $output;
}
/**
 * Buttons with SVG icons
 *
 * @uses  house_svg_icon()
 *
 * @param  string $text       Button text
 * @param  string $url        Button url
 * @param  string $icon       Name of icon
 * @param  string $icon_class Class for the icon
 * @param  string $btn_class  Class for the button
 * @param  string $id         ID for the button
 * @param  string $text_pos   Text position relative to icon, default is left
 * @return string             Returns button markup
 */
function house_svg_icon_button( $text = 'Button text', $url = '', $icon = '', $icon_class = '', $btn_class = '', $id = '', $text_pos = 'left' ) {
	$svg = house_svg_icon( $icon, $icon_class );

	$output = '';
	$output .= '<a id="' . $id . '" href="' . $url . '" class="btn ' . $btn_class . '">';

	if ( $text_pos === 'left' ) {
		$output .= $text . ' ' . $svg;
	} elseif ( $text_pos === 'right' ) {
		$output .= $svg . ' ' . $text;
	}

	$output .= '</a>';

	return $output;
}
/**
 * Primary button
 *
 * @param  string $text  		Text for the button
 * @param  string $url   		URL for the button
 * @param  string $btn_class 	Extra class (besides .btn and .btn--primary)
 * @param  string $target 		Link target attribute, default is self
 * @return string        		Returns button markup
 */
function house_primary_button( $text = 'Button text', $url = '', $btn_class = '', $target = "_self" ) {

	$output = '<a href="' . $url . '" class="btn btn--primary ' . $btn_class . '" target="' . $target . '">' . $text . '</a>';

	return $output;
}