/**
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */

( function( $ ) {

	// Site title
	wp.customize( 'blogname', function( value ) {
		value.bind( function( to ) {
			$( '.site-title a' ).text( to );
		} );
	} );
	// Site description
	wp.customize( 'blogdescription', function( value ) {
		value.bind( function( to ) {
			$( '.site-description' ).text( to );
		} );
	} );
	// Header custom text
	wp.customize( 'header_custom_text', function( value ) {
		value.bind( function( to ) {
			$( '.header-custom-text' ).text( to );
		} );
	} );
	// Footer copyrights
	wp.customize( 'footer_copyright_text', function( value ) {
		value.bind( function( to ) {
			$( '#colophon' ).text( to );
		} );
	} );

} )( jQuery );
