<?php
/**
 * Customizer for House theme
 *
 * @package WordPress
 */
/**
 * Register customizer
 *
 * Adds postMessage support for site title and description for the Customizer.
 * This function is attached to 'customize_register' action hook.
 *
 * @param WP_Customize_Manager $wp_customize The Customizer object.
 */
function house_customize_register( $wp_customize ) {
	/**
	 * Failsafe is safe
	 */
	if ( ! isset( $wp_customize ) ) {
		return;
	}

	/**
	 * Add Theme Options panel
	 *
	 * @uses $wp_customize->add_panel() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_panel/
	 * @link $wp_customize->add_panel() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_panel
	 */
	$wp_customize->add_panel( 'house_options_panel', array(
		'title'       => __( 'Theme Options', 'house' ),
		'description' => __( 'Configure your theme settings', 'house' ),
	) );

	/**
	 * Add Header Section for Theme Options.
	 *
	 * @uses $wp_customize->add_section() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_section/
	 * @link $wp_customize->add_section() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_section
	 */
	$wp_customize->add_section( 'header_options_section', array(
		'title' => __( 'Header options', 'house' ),
		'panel' => 'house_options_panel',
	) );

	/**
	 * Add Footer Section for Theme Options.
	 *
	 * @uses $wp_customize->add_section() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_section/
	 * @link $wp_customize->add_section() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_section
	 */
	$wp_customize->add_section( 'footer_options_section', array(
		'title' => __( 'Footer options', 'house' ),
		'panel' => 'house_options_panel',
	) );

	/**
	 * Add Social profiles links for Theme Options.
	 *
	 * @uses $wp_customize->add_section() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_section/
	 * @link $wp_customize->add_section() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_section
	 */
	$wp_customize->add_section( 'social_profiles_section', array(
		'title' => __( 'Social Profiles', 'house' ),
		'panel' => 'house_options_panel',
	) );

	/**
	 * Add MailChimp Section for Theme Options.
	 *
	 * @uses $wp_customize->add_section() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_section/
	 * @link $wp_customize->add_section() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_section
	 */
	$wp_customize->add_section( 'mailchimp_options_section', array(
		'title' => __( 'MailChimp options', 'house' ),
		'panel' => 'house_options_panel',
	) );

	/**
	 * Header Custom Text setting
	 *
	 * - Setting: Header Custom Text
	 * - Control: text
	 * - Sanitization: text
	 *
	 * Uses a text field to configure user's text displayed in the site header.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'header_custom_text', array(
		'default'           => '',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'transport'         => 'postMessage',
		'sanitize_callback' => 'house_sanitize_nohtml'
	) );

	/**
	 * Header Custom Text control
	 *
	 * - Control: Basic: Text
	 * - Setting: Header Custom Text
	 * - Sanitization: text
	 *
	 * Register the core "text" control to be used to configure the Header Custom Text setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'header_custom_text', array(
		'label'       => __( 'Header Custom Text', 'house' ),
		'description' => __( 'Text to appear somewhere in header.', 'house' ),
		'section'     => 'header_options_section',
		'settings'    => 'header_custom_text',
		'type'        => 'text',
	) );

	/**
	 * Footer Copyright Text setting
	 *
	 * - Setting: Footer Copyright Text
	 * - Control: text
	 * - Sanitization: text
	 *
	 * Uses a text field to configure the user's copyright text displayed in the site footer.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'footer_copyright_text', array(
		'default'           => sprintf( __( '%2$s Copyright %1$s. All rights reserved.', 'house' ),
								esc_html( get_bloginfo( 'name' ) ), date( 'Y' ) ),
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'transport'         => 'postMessage',
		'sanitize_callback' => 'house_sanitize_nohtml'
	) );

	/**
	 * Footer Copyright Text control
	 *
	 * - Control: Basic: Text
	 * - Setting: Footer Copyright Text
	 * - Sanitization: text
	 *
	 * Register the core "text" control to be used to configure the Footer Copyright Text setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'footer_copyright_text', array(
		'label'       => __( 'Footer Copyright Text', 'house' ),
		'description' => __( 'Copyright text for the site footer.', 'house' ),
		'section'     => 'footer_options_section',
		'settings'    => 'footer_copyright_text',
		'type'        => 'text',
	) );

	/**
	 * Social Profiles Facebook
	 *
	 * - Setting: Social Profiles Facebook
	 * - Control: url
	 * - Sanitization: url
	 *
	 * Uses a text field to configure the user's copyright text displayed in the site footer.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'social_profiles_facebook', array(
		'default'           => '',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'house_sanitize_url'
	) );

	/**
	 * Facebook control
	 *
	 * - Control: Text: URL
	 * - Setting: Social Profiles Facebook
	 * - Sanitization: url
	 *
	 * Register the core "url" text control to be used to configure the Social Profiles setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'social_profiles_facebook', array(
		'label'       => __( 'Facebook', 'house' ),
		'description' => __( 'Add your Facebook profile link.', 'house' ),
		'section'     => 'social_profiles_section',
		'settings'    => 'social_profiles_facebook',
		'type'        => 'url',
	) );

	/**
	 * Social Profiles Twitter
	 *
	 * - Setting: Social Profiles Twitter
	 * - Control: url
	 * - Sanitization: url
	 *
	 * Uses a text field to configure the user's copyright text displayed in the site footer.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'social_profiles_twitter', array(
		'default'           => '',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'house_sanitize_url'
	) );

	/**
	 * Twitter control
	 *
	 * - Control: Text: URL
	 * - Setting: Social Profiles
	 * - Sanitization: url
	 *
	 * Register the core "url" text control to be used to configure the Social Profiles setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'social_profiles_twitter', array(
		'label'       => __( 'Twitter', 'house' ),
		'description' => __( 'Add your Twitter profile link.', 'house' ),
		'section'     => 'social_profiles_section',
		'settings'    => 'social_profiles_twitter',
		'type'        => 'url',
	) );

	/**
	 * Social Profiles Instagram
	 *
	 * - Setting: Social Profiles Instagram
	 * - Control: url
	 * - Sanitization: url
	 *
	 * Uses a text field to configure the user's copyright text displayed in the site footer.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'social_profiles_instagram', array(
		'default'           => '',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'house_sanitize_url'
	) );

	/**
	 * Instagram control
	 *
	 * - Control: Text: URL
	 * - Setting: Social Profiles
	 * - Sanitization: url
	 *
	 * Register the core "url" text control to be used to configure the Social Profiles setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'social_profiles_instagram', array(
		'label'       => __( 'Instagram', 'house' ),
		'description' => __( 'Add your Instagram profile link.', 'house' ),
		'section'     => 'social_profiles_section',
		'settings'    => 'social_profiles_instagram',
		'type'        => 'url',
	) );

	/**
	 * Social Profiles Google plus
	 *
	 * - Setting: Social Profiles Google plus
	 * - Control: url
	 * - Sanitization: url
	 *
	 * Uses a text field to configure the user's copyright text displayed in the site footer.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'social_profiles_google', array(
		'default'           => '',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'house_sanitize_url'
	) );

	/**
	 * Google plus control
	 *
	 * - Control: Text: URL
	 * - Setting: Social Profiles
	 * - Sanitization: url
	 *
	 * Register the core "url" text control to be used to configure the Social Profiles setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'social_profiles_google', array(
		'label'       => __( 'Google plus', 'house' ),
		'description' => __( 'Add your Google plus profile link.', 'house' ),
		'section'     => 'social_profiles_section',
		'settings'    => 'social_profiles_google',
		'type'        => 'url',
	) );

	/**
	 * Social Profiles Pinterest
	 *
	 * - Setting: Social Profiles Pinterest
	 * - Control: url
	 * - Sanitization: url
	 *
	 * Uses a text field to configure the user's copyright text displayed in the site footer.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'social_profiles_pinterest', array(
		'default'           => '',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'house_sanitize_url'
	) );

	/**
	 * Pinterest control
	 *
	 * - Control: Text: URL
	 * - Setting: Social Profiles
	 * - Sanitization: url
	 *
	 * Register the core "url" text control to be used to configure the Social Profiles setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'social_profiles_pinterest', array(
		'label'       => __( 'Pinterest', 'house' ),
		'description' => __( 'Add your Pinterest profile link.', 'house' ),
		'section'     => 'social_profiles_section',
		'settings'    => 'social_profiles_pinterest',
		'type'        => 'url',
	) );

	/**
	 * Social Profiles YouTube
	 *
	 * - Setting: Social Profiles YouTube
	 * - Control: url
	 * - Sanitization: url
	 *
	 * Uses a text field to configure the user's copyright text displayed in the site footer.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'social_profiles_youtube', array(
		'default'           => '',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'house_sanitize_url'
	) );

	/**
	 * YouTube control
	 *
	 * - Control: Text: URL
	 * - Setting: Social Profiles
	 * - Sanitization: url
	 *
	 * Register the core "url" text control to be used to configure the Social Profiles setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'social_profiles_youtube', array(
		'label'       => __( 'YouTube', 'house' ),
		'description' => __( 'Add your YouTube profile link.', 'house' ),
		'section'     => 'social_profiles_section',
		'settings'    => 'social_profiles_youtube',
		'type'        => 'url',
	) );

	/**
	 * Social Profiles Vimeo
	 *
	 * - Setting: Social Profiles Vimeo
	 * - Control: url
	 * - Sanitization: url
	 *
	 * Uses a text field to configure the user's copyright text displayed in the site footer.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'social_profiles_vimeo', array(
		'default'           => '',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'house_sanitize_url'
	) );

	/**
	 * Vimeo control
	 *
	 * - Control: Text: URL
	 * - Setting: Social Profiles
	 * - Sanitization: url
	 *
	 * Register the core "url" text control to be used to configure the Social Profiles setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'social_profiles_vimeo', array(
		'label'       => __( 'Vimeo', 'house' ),
		'description' => __( 'Add your Vimeo profile link.', 'house' ),
		'section'     => 'social_profiles_section',
		'settings'    => 'social_profiles_vimeo',
		'type'        => 'url',
	) );

	/**
	 * Mailchimp API Key setting
	 *
	 * - Setting: Mailchimp API Key
	 * - Control: text
	 * - Sanitization: text
	 *
	 * Uses a text field to configure the Mailchimp API Key.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'mailchimp_api_key', array(
		'default'           => '',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'house_sanitize_nohtml'
	) );

	/**
	 * Mailchimp API Key control
	 *
	 * - Control: Basic: Text
	 * - Setting: Mailchimp API Key
	 * - Sanitization: text
	 *
	 * Register the core "text" control to be used to configure the Mailchimp API Key setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'mailchimp_api_key', array(
		'label'       => __( 'Mailchimp API Key', 'house' ),
		'description' => __( 'Paste your Mailchimp API Key here.', 'house' ),
		'section'     => 'mailchimp_options_section',
		'settings'    => 'mailchimp_api_key',
		'type'        => 'text',
	) );

	/**
	 * Mailchimp List ID setting
	 *
	 * - Setting: Mailchimp List ID
	 * - Control: text
	 * - Sanitization: text
	 *
	 * Uses a text field to configure the Mailchimp List ID.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'mailchimp_list_id', array(
		'default'           => '',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'house_sanitize_nohtml'
	) );

	/**
	 * Mailchimp List ID control
	 *
	 * - Control: Basic: Text
	 * - Setting: Mailchimp List ID
	 * - Sanitization: text
	 *
	 * Register the core "text" control to be used to configure the Mailchimp List ID setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'mailchimp_list_id', array(
		'label'       => __( 'Mailchimp List ID', 'house' ),
		'description' => __( 'Paste your Mailchimp List ID here.', 'house' ),
		'section'     => 'mailchimp_options_section',
		'settings'    => 'mailchimp_list_id',
		'type'        => 'text',
	) );

	/**
	 * Mailchimp Thank you message setting
	 *
	 * - Setting: Mailchimp Thank you message
	 * - Control: text
	 * - Sanitization: text
	 *
	 * Uses a text field to configure the Mailchimp Thank you message.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'mailchimp_thank_you_message', array(
		'default'           => __( 'Thank you for subscribing.', 'house' ),
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'house_sanitize_nohtml'
	) );

	/**
	 * Mailchimp Thank you message control
	 *
	 * - Control: Basic: Text
	 * - Setting: Mailchimp Thank you message
	 * - Sanitization: text
	 *
	 * Register the core "text" control to be used to configure the Mailchimp Thank you message setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'mailchimp_thank_you_message', array(
		'label'       => __( 'Mailchimp "Thank you" message', 'house' ),
		'description' => __( 'Wtite your "Thank you" message as success feedback for subscriber.', 'house' ),
		'section'     => 'mailchimp_options_section',
		'settings'    => 'mailchimp_thank_you_message',
		'type'        => 'text',
	) );

	/**
	 * Mailchimp Empty field message setting
	 *
	 * - Setting: Mailchimp Empty field message
	 * - Control: text
	 * - Sanitization: text
	 *
	 * Uses a text field to configure the Mailchimp Empty field message.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'mailchimp_empty_field_message', array(
		'default'           => __( 'Address field is empty. Please type your email address and try submitting again.', 'house' ),
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'house_sanitize_nohtml'
	) );

	/**
	 * Mailchimp Empty field message control
	 *
	 * - Control: Basic: Text
	 * - Setting: Mailchimp Empty field message
	 * - Sanitization: text
	 *
	 * Register the core "text" control to be used to configure the Mailchimp Empty field message setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'mailchimp_empty_field_message', array(
		'label'       => __( 'Mailchimp "Empty field" message', 'house' ),
		'description' => __( 'Wtite your "Empty field" message as error feedback for subscriber.', 'house' ),
		'section'     => 'mailchimp_options_section',
		'settings'    => 'mailchimp_empty_field_message',
		'type'        => 'textarea',
	) );

	/**
	 * Set Selective Refresh for blog name and description
	 *
	 * All settings with postMessage transport need custom javascript
	 * definitions as well. @see js/customizer.js
	 */
	$wp_customize->get_setting( 'blogname' )->transport        = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		/**
		 * Site name
		 */
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector'            => '.site-title a',
			'container_inclusive' => false,
			'render_callback'     => 'house_customize_partial_blogname',
		));
		/**
		 * Site description
		 */
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector'            => '.site-description',
			'container_inclusive' => false,
			'render_callback'     => 'house_customize_partial_blogdescription',
		));
		/**
		 * Header custom text
		 */
		$wp_customize->selective_refresh->add_partial( 'header_custom_text', array(
			'selector'            => '.header-custom-text',
			'container_inclusive' => false,
			'render_callback'     => 'house_customize_partial_header_custom_text',
		));
		/**
		 * Footer copyrights
		 */
		$wp_customize->selective_refresh->add_partial( 'footer_copyright_text', array(
			'selector'            => '#colophon',
			'container_inclusive' => false,
			'render_callback'     => 'house_customize_partial_footer_copyright_text',
		));
	}
}

add_action( 'customize_register', 'house_customize_register', 11 );

/**
 * Render the site title for the selective refresh partial.
 *
 * @see house_customize_register()
 *
 * @return void
 */
function house_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @see house_customize_register()
 *
 * @return void
 */
function house_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Render the header custom text for the selective refresh partial.
 *
 * @see house_customize_register()
 *
 * @return void
 */
function house_customize_partial_header_custom_text() {
	echo get_theme_mod( 'header_custom_text' );
}
/**
 * Render the footer copyrights for the selective refresh partial.
 *
 * @see house_customize_register()
 *
 * @return void
 */
function house_customize_partial_footer_copyright_text() {
	echo get_theme_mod( 'footer_copyright_text' );
}
/**
 * No-HTML sanitization callback
 *
 * - Sanitization: nohtml
 * - Control: text, textarea, password
 *
 * Sanitization callback for 'nohtml' type text inputs. This callback sanitizes `$nohtml`
 * to remove all HTML.
 *
 * NOTE: wp_filter_nohtml_kses() can be passed directly as `$wp_customize->add_setting()`
 * 'sanitize_callback'. It is wrapped in a callback here merely for example purposes.
 *
 * @see wp_filter_nohtml_kses() https://developer.wordpress.org/reference/functions/wp_filter_nohtml_kses/
 *
 * @param string $nohtml The no-HTML content to sanitize.
 * @return string Sanitized no-HTML content.
 */
function house_sanitize_nohtml( $nohtml ) {
	return wp_filter_nohtml_kses( $nohtml );
}
/**
 * URL sanitization callback
 *
 * - Sanitization: url
 * - Control: text, url
 *
 * Sanitization callback for 'url' type text inputs. This callback sanitizes `$url` as a valid URL.
 *
 * NOTE: esc_url_raw() can be passed directly as `$wp_customize->add_setting()` 'sanitize_callback'.
 * It is wrapped in a callback here merely for example purposes.
 *
 * @see esc_url_raw() https://developer.wordpress.org/reference/functions/esc_url_raw/
 *
 * @param string $url URL to sanitize.
 * @return string Sanitized URL.
 */
function house_sanitize_url( $url ) {
	return esc_url_raw( $url );
}

/**
 * Customize preview init
 *
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 * This function is attached to 'customize_preview_init' action hook.
 */
function house_customize_preview_js() {
	wp_enqueue_script( 'house-customizer', get_template_directory_uri() . '/inc/customizer/js/customizer.js', array( 'jquery','customize-preview' ), '1.0.0', true );
}
add_action( 'customize_preview_init', 'house_customize_preview_js' );