<?php
/**
 * Excerpts modifications
 *
 * @package WordPress
 */
/**
 * Hooks
 */
add_action( 'init', 'house_enable_page_excerpt' );
add_filter( 'excerpt_length', 'house_excerpt_length', 999 );
add_filter( 'get_the_excerpt', 'house_custom_excerpt_more' );
add_filter( 'excerpt_more', 'house_auto_excerpt_more' );
add_filter( 'the_content_more_link', 'house_more_link_url' );
/**
 * Excerpt in pages
 *
 * Add support for excerpt in pages. This function is attached to 'init' action hook.
 *
 * @uses add_post_type_support() 		Register support of certain features for a post type. *
 * @param string       	$post_type 		The post type for which to add the feature.
 * @param string|array 	$feature   		The feature being added, accpets an array of
 * 									    feature strings or a single string.
 *
 * @link https://core.trac.wordpress.org/browser/tags/3.9.1/src/wp-includes/post.php
 *
 */
function house_enable_page_excerpt() {
	add_post_type_support( 'page', 'excerpt' );
}
/**
 * Excerpt length
 *
 * Sets the post excerpt length to 40 words. This function is attached to 'excerpt_length'
 * filter hook.
 *
 * @uses wp_trim_excerpt()
 * @param integer 		$lenght		The number of words returned in excerpt. Default is 55.
 * @return string
 * @link http://codex.wordpress.org/Plugin_API/Filter_Reference/excerpt_length
 * @link https://core.trac.wordpress.org/browser/tags/3.9.1/src/wp-includes/formatting.php
 */
function house_excerpt_length( $length ) {
	return 40;
}
/**
 * Continue Reading link
 *
 * Returns a "Continue Reading" link for excerpts. This function uses function_exists() call
 * for easier overriding in child themes.
 * @see house_custom_excerpt_more()
 * @see house_auto_excerpt_more()
 * @return string
 */
function house_continue_reading_link() {
	return ' <a href="'. esc_url( get_permalink() ) . '" class="more-link">' . __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'house' ) . '</a>';
}
/**
 * Continue Reading link for custom post excerpts
 *
 * Adds a pretty "Continue Reading" link to custom post excerpts. This function is attached to
 * 'get_the_excerpt' filter hook.
 *
 * @see house_continue_reading_link()
 * @uses get_the_excerpt()
 * @param string 	$output 		Content of the excerpt.
 * @return string
 * @link http://codex.wordpress.org/Plugin_API/Filter_Reference/get_the_excerpt
 * @link https://core.trac.wordpress.org/browser/tags/3.9.1/src/wp-includes/post-template.php
 */
function house_custom_excerpt_more( $output ) {
	if ( has_excerpt() && ! is_attachment() ) {
		$output .= house_continue_reading_link();
	}
	return $output;
}
/**
 * Replace "[...]"
 *
 * Replaces "[...]" (appended to automatically generated excerpts) with an ellipsis and "Continue Reading" link.
 * This function is attached to 'excerpt_more' filter hook.
 * @see house_continue_reading_link()
 * @uses wp_trim_excerpt()
 * @param string 	$more 		Excerpt more text/link.
 * @return string
 * @link http://codex.wordpress.org/Plugin_API/Filter_Reference/excerpt_more
 * @link https://core.trac.wordpress.org/browser/tags/3.9.1/src/wp-includes/formatting.php
 */
function house_auto_excerpt_more( $more ) {
	return ' &hellip;' . house_continue_reading_link();
}
/**
 * More link destination
 *
 * Change more link url from '#more-*' to permalink. This function is attached to 'the_content_more_link'
 * filter hook.
 *
 * @param string 	$link 	More link
 * @return string
 * @link http://developer.wordpress.org/reference/hooks/the_content_more_link/
 */
function house_more_link_url( $link ) {
	$offset = strpos( $link, '#more-' );

	if ( $offset ) {
		$end = strpos( $link, '"', $offset );
	}
	if ( $end ) {
		$link = substr_replace( $link, '',  $offset, $end-$offset );
	}

	return $link;
}