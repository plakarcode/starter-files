<?php
/**
 * YouTube API functions
 *
 * @package WordPress
 */
/**
 * Get YouTube video id
 *
 * @link http://stackoverflow.com/questions/3392993/php-regex-to-get-youtube-video-id#answer-17030234
 *
 * @param  string $url YouTube video url
 * @return string      Returns video ID
 */
function get_youtube_video_id( $url ) {
	preg_match( "/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $url, $matches);
	$id = $matches[1];

	return $id;
}
/**
 * Get youtube video thumbnail
 *
 * Get the video thumbnail from id.
 *
 * To get stipless images (for 16:9), urls are:
 * http://img.youtube.com/vi/VIDEO_ID/mqdefault.jpg (320x180)
 * http://img.youtube.com/vi/VIDEO_ID/maxresdefault.jpg (1500x900)
 *
 * @link http://stackoverflow.com/questions/13220715/removing-black-borders-43-on-youtube-thumbnails#answer-18978874
 * @param  string $url     YouTube video url
 * @return string          Returns src for the thumbnail
 */
function get_youtube_thumbnail( $url ) {
	$id = get_youtube_video_id( $url );
	$src = 'http://img.youtube.com/vi/' . $id . '/mqdefault.jpg';

	return $src;
}
/**
 * YouTube video thumbnail
 *
 * Echo YouTube video thumbnail url.
 *
 * @param  string $url     YouTube video url
 * @return string          Echoes src for the thumbnail
 */
function youtube_thumbnail( $url ) {
	echo get_youtube_thumbnail( $url );
}
/**
 * Get youtube video full image
 *
 * Get the video image from id.
 *
 * To get stipless images (for 16:9), urls are:
 * http://img.youtube.com/vi/VIDEO_ID/mqdefault.jpg (320x180)
 * http://img.youtube.com/vi/VIDEO_ID/maxresdefault.jpg (1500x900)
 *
 * @link http://stackoverflow.com/questions/13220715/removing-black-borders-43-on-youtube-thumbnails#answer-18978874
 * @param  string $url     YouTube video url
 * @return string          Returns src for the thumbnail
 */
function get_youtube_image( $url ) {
	$id = get_youtube_video_id( $url );
	$src = 'http://img.youtube.com/vi/' . $id . '/maxresdefault.jpg';

	return $src;
}
/**
 * YouTube video full image
 *
 * Echo YouTube video full image url.
 *
 * @param  string $url     YouTube video url
 * @return string          Echoes src for the thumbnail
 */
function youtube_image( $url ) {
	echo get_youtube_image( $url );
}