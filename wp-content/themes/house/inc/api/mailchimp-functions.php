<?php
/**
 * MailChimp functions
 *
 * Custom functionality for MailChimp integration
 *
 * Uses third party script 'MailChimp API'.
 * @link https://packagist.org/packages/drewm/mailchimp-api
 * @link https://github.com/drewm/mailchimp-api/
 *
 * @package WordPress
 * @subpackage MailChimp API
 */

/**
 * These lines are mandatory.
 */
$file = get_template_directory() . '/inc/api/vendor/autoload.php';

/**
 * Let's not kill all if someone forgot to run composer,
 * or is frontend :P
 */
if ( ! file_exists( $file ) ) {
	return;
}

require_once( $file );

use \DrewM\MailChimp\MailChimp;

/**
 * Get MailChimp response
 *
 * Get the user email address input and either add it to the MailChimp list or
 * return error.
 *
 * @param  string $list_id   MailChimp list id
 * @param  string $email     User input email address
 * @param  string $apikey    MailChimp API key
 * @return array             Returns either error or adds new subscriber to the list
 */
function get_mailchimp_response( $list_id = '', $email = '', $apikey = '' ) {

	$MailChimp = new MailChimp( $apikey );

	$response = $MailChimp->post( "lists/$list_id/members", [
					'email_address' => "$email",
					'status'        => 'subscribed',
				]);

	return $response;
}

// require_once( $file );

// use \DrewM\MailChimp\MailChimp;

// // API Key needed
// $MailChimp = new MailChimp('abc123abc123abc123abc123abc123-us1');

// $result = $MailChimp->get('lists');

