<?php
/**
 * Device functions
 *
 * Custom functionality for detecting mobile devices.
 *
 * Uses third party script 'Mobile Detect'.
 * @link        Homepage:     http://mobiledetect.net
 *              GitHub Repo:  https://github.com/serbanghita/Mobile-Detect
 *              Google Code:  http://code.google.com/p/php-mobile-detect/
 *              README:       https://github.com/serbanghita/Mobile-Detect/blob/master/README.md
 *              HOWTO:        https://github.com/serbanghita/Mobile-Detect/wiki/Code-examples
 *
 * @link https://github.com/serbanghita/Mobile-Detect/wiki/Code-examples
 * @see inc/api/vendor/mobiledetect/mobiledetectlib/examples/demo.php
 *
 * @package WordPress
 * @subpackage Mobile Detect
 */

/**
 * These lines are mandatory.
 */
$file = get_template_directory() . '/inc/api/vendor/mobiledetect/mobiledetectlib/Mobile_Detect.php';

/**
 * Let's not kill all if someone forgot to run composer,
 * or is frontend :P
 */
if ( ! file_exists( $file ) ) {
	return;
}

// require_once( $file );

// $detect = new Mobile_Detect;
