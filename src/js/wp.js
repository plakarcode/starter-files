'use strict';
/**
 * wp.js
 *
 * Some things needs extra tweaks in WordPress
 */
var $ = window.jQuery;

/**
 * Admin bar gets very annoying overlapping with
 * fixed header
 */
function adminBarHeight() {
	var $bar = $('#wpadminbar').outerHeight();

	$('#mainheader').css('margin-top', $bar);
}

//ON DOCUMENT READY
$(document).ready(function() {

	if ( $('#wpadminbar').length ) {
		adminBarHeight();
	}
});


//WINDOW ONLOAD
$(window).load(function() {

  // WINDOW RESIZE
  $(window).on('resize', function() {

  }).trigger('resize');

});
