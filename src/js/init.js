'use strict';

var $ = window.jQuery;

//ON DOCUMENT READY
$(document).ready(function() {

	//SVG for Everybody (ie9+, ...)
	svg4everybody();
});


//WINDOW ONLOAD
$(window).load(function() {

  // WINDOW RESIZE
  $(window).on('resize', function() {

  }).trigger('resize');

});
