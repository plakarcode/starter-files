# Site global branding

## Logo markup

```
<a href="{url}" class="custom-logo-link" rel="home" itemprop="url">
	<img width="{width}" height="{height}" src="{src}" class="custom-logo" alt="" itemprop="logo"
		srcset="{src width}, {src width}" sizes="(max-width: {width}) 100vw, {width}">
</a>
```

## Site title

On home page:

```
<h1 class="site-title"><a href="{url}" rel="home">{Site Title}</a></h1>
```

On all other places:

```
<p class="site-title"><a href="{url}" rel="home">{Site Title}</a></p>
```

## Site description

```
<p class="site-description">{Just another WordPress site}</p>
```

# Menu

Classes with the most importance to us:

- `.menu` - Main ul.
- `.sub-menu` - Submenu.

- `.menu-item-home` - This class is added to menu items that correspond to the site front page.
- `.menu-item` - This class is added to every menu item.
- `.menu-item-has-children` - This class is added to menu item which has sub-items.
- `.current-menu-item` - This class is added to menu items that correspond to the currently rendered page.
- `.current-menu-parent` - This class is added to menu items that correspond to the hierarchical parent of the currently rendered page.
- `.current-menu-ancestor` - This class is added to menu items that correspond to a hierarchical ancestor of the currently rendered page.

## Please pay attention to the fact that links have no classes, only `<ul>` and `<li>` tags have.

For more info do take a look [here](https://developer.wordpress.org/reference/functions/wp_nav_menu/#menu-item-css-classes).

```
<div class="">
	<ul id="" class="menu">
		<li id="menu-item-{id}" class="menu-item menu-item-home menu-item-type-custom menu-item-object-custom menu-item-{id}">
			<a href="{src}">{Page Title}</a>
		</li>
		<li id="menu-item-{id}" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-{id}">
			<a href="{src}">{Page Title}</a>
		</li>
		<li id="menu-item-{id}" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-{id} current_page_item menu-item-{id}">
			<a href="{src}">{Page Title}</a>
		</li>
		<li id="menu-item-{id}" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-{id}">
			<a href="{src}">{Page Title}</a>
			<ul class="sub-menu">
				<li id="menu-item-{id}" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-{id}">
					<a href="{src}">{Page Title}</a>
				</li>
				<li id="menu-item-{id}" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-{id}">
					<a href="{src}">{Page Title}</a>
				</li>
				<li id="menu-item-{id}" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-{id}">
					<a href="{src}">{Page Title}</a>
				</li>
				<li id="menu-item-{id}" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-{id}">
					<a href="{src}">{Page Title}</a>
				</li>
				<li id="menu-item-{id}" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-{id}">
					<a href="{src}">{Page Title}</a>
				</li>
			</ul><!-- sub-menu -->
		</li><!-- menu-item-has-children -->
		<li id="menu-item-{id}" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-{id}">
			<a href="{src}">{Page Title}</a>
			<ul class="sub-menu">
				<li id="menu-item-{id}" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-{id}">
					<a href="{src}">{Page Title}</a>
					<ul class="sub-menu">
						<li id="menu-item-{id}" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-{id}">
							<a href="{src}">{Page Title}</a>
						</li>
						<li id="menu-item-{id}" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-{id}">
							<a href="{src}">{Page Title}</a>
						</li>
						<li id="menu-item-{id}" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-{id}">
							<a href="{src}">{Page Title}</a>
						</li>
					</ul><!-- sub-menu -->
				</li><!-- menu-item-has-children -->
				<li id="menu-item-{id}" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-{id}">
					<a href="{src}">{Page Title}</a>
				</li>
				<li id="menu-item-{id}" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-{id}">
					<a href="{src}">{Page Title}</a>
				</li>
			</ul><!-- sub-menu -->
		</li><!-- menu-item-has-children -->
		<li id="menu-item-{id}" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-{id}">
			<a href="{src}">{Page Title}</a>
		</li>
		<li id="menu-item-{id}" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-{id}">
			<a href="{src}">{Page Title}</a>
		</li>
		<li id="menu-item-{id}" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-{id}">
			<a href="{src}">{Page Title}</a>
		</li>
	</ul><!-- menu -->
</div>
```