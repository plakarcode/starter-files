# Tech stack

+ node.js and node package manager [npm](https://www.npmjs.org/)
+ Inuitcss
+ Jquery
+ Compass (SCSS)
+ PHP dependency manager [Composer](https://getcomposer.org/)
+ WordPress
+ [WP CLI](http://wp-cli.org/) - (optional)


# Getting Started

This is guideline only for setting up and testing this repo - not for the actual project. **You have been warned.**


## 1. PreInstallation Settings

+ Clone this repo to your local server folder
+ Create new sql database
+ Set new virtual host (or whatever you use for local domains) for newly created dir

*_Setting up virtual host and sql database is, obviously, optional for frontend devs._


## 2. Installation with WP CLI
_Or install the old way and skip this part._

Navigate to project' dir and download wp:

```shell
$ wp core download
```

Set config file (config file is ignored by .gitignore so just use your local settings):

```shell
$ wp core config --dbname=DBNAME --dbuser=DBUSER --dbpass=DBPASS
```

Install WordPress:

```shell
$ wp core install --url=LOCALURL --title=TITLE --admin_user=USERNAME --admin_password=PASSWORD --admin_email=EMAIL@EXAMPLE.COM
```

## 3. Build Project (all this is done from project's root)

Make sure you have the latest node.js version. If not, you'll get the `ReferenceError: Promise is not defined` error after `gulp build` command. In this case do check [this link](http://stackoverflow.com/questions/32490328/gulp-autoprefixer-throwing-referenceerror-promise-is-not-defined#answer-32502195).

If you don't have bower and gulp installed globally you'll need to run (`sudo` is for us with Linux):

```shell
$ sudo npm install -g bower
$ sudo npm install -g gulp
```

After that, from the project's directory run:

```shell
$ sudo npm cache clean && npm update --save
$ bower install
```

To build the project, from the project's directory run:

```shell
$ gulp build
$ gulp watch
```

if you have problems with 'npm install' command on windows, update npm

1. go to c:\Users\name\AppData\Roaming\npm
2. delete files 'npm', 'npm.cmd'
3. npm install -g npm@next
4. try to run 'npm install'

To build third party `php` scripts, from the project's directory run:

```shell
$ composer install
```

Quick composer install - [https://getcomposer.org/download/](https://getcomposer.org/download/)

## 4. Setup theme

Activate theme

```shell
wp theme activate house
```
_Or just go to browser, login to dashboard and change theme :)_


## 5. Required plugins

Login to dashboard and visit **Appearance -> Required Plugins**. Install and activate all required plugins (some, if not all, of them will be activated automatically with installation).

## 6. Paths

Frontend: `{_project_root_}/wp-content/themes/house/markup/index.html`

Backend: `{_project_root_/}` (virtual host)

Woohoo! Happy coding!
